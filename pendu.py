#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 13:51:01 2020

@author: 1Ã¨re nsi
"""
#apenser:'a'.upper()
import random
q=""
#fonction de dessinPendu
def dessinPendu(nb):
    tab=[
    """
       +-------+
       |
       |
       |
       |
       |
    ==============
    """,
    """
       +-------+
       |       |
       |       O
       |
       |
       |
    ==============
    """
        ,
    """
       +-------+
       |       |
       |       O
       |       |
       |
       |
    ==============
    """,
    """
       +-------+
       |       |
       |       O
       |      -|
       |
       |
    ==============
    """,
    """
       +-------+
       |       |
       |       O
       |      -|-
       |
       |
    ==============
    """,
    """
       +-------+
       |       |
       |       O
       |      -|-
       |      |
       |
    ==============
    """,
    """
       +-------+
       |       |
       |       O
       |      -|-
       |      | |
       |
    ==============
    """
    ]
    return tab[nb]
#fonction de recherche
def search():
    #liste pour les mots du dico.txt
    mots=[]
    #variable indiquand la fin
    ends=False
    #nombre de lettre fausse
    fail = 0
    #nombre de lettre trouvé
    nbrl=0
    #mettre tous les mots du dico.txt dans la liste mots
    with open('dico.txt','r') as f :
        for ligne in f:
            ligne=ligne.replace("\n","")
            mots.append(ligne)
    #choisir un mot au hazard de la liste
    choi = random.randint(0,len(mots)-1)
    mot = list(mots[choi])
    #affichage du nombre de lettre
    print ("le mot a ", len(mot)," lettres")
    #liste pour la liste qui contient les mots trouvés
    letf=[]
    #ajout d'un tiret pour chaque lettre
    for x in range (len(mot)):
        letf.append("_")
    #affichage des tirets au nombre de lettre
    print (" ".join(letf))
    #boucle qui s'execute tant que le joueur n'a ni perdu ni gagner
    while ends==False:
        #On demande de donnez une lettre
        ch=input("Donnez une lettre: ")
        #pour vérifier si une lettre a été trouvé
        nbr=0
        #On vérifie si la lettre correspond a une du mot si oui nbr!=0 
        for x in range (len(mot)):
            if mot[x]==ch.upper():
                #on rajoute 1 a chaque nombre de lettre trouvé
                nbr=nbr +1
                nbrl=nbrl+1
                letf[x]=ch.upper()
        #si aucune lettre a été trouvé on ajoute un à fail et on execute la fonction dessin pendu
        if nbr==0:
            print (dessinPendu(fail))
            fail=fail+1
        #on affiche les lettre trouvé et des tirets pour les lettres non trouvé
        print (" ".join(letf))
        #Si 7 lettre fausse alors c'est la fin, on affiche un message et le mot qui était recherché
        if fail==7:
            ends = True
            print("""
***********************
****VOUS AVEZ PERDU****
***********************

le mot étais: """," ".join(mot))
        #si toutes les lettres sont trouvé c'est gagné et on affiche bien joue    
        if nbrl==len(mot):
            ends=True
            print("""
******************
***BIEN JOUE!!****
******************""")
#menu principale
while (q!="q")and(q!='0'):
    q = input("""
Tapez 'q'ou '0' pour quitter
Tapez '1' pour rechercher un mot  dans la liste

Votre choix: """)
    
    if q == '1':
        search()
    elif q=='q' or q=='0':
        print("""
********************
******AU REVOIR*****
********************
              """)